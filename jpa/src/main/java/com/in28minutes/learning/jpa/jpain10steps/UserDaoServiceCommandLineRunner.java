/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.in28minutes.learning.jpa.jpain10steps;

import com.in28minutes.learning.jpa.jpain10steps.entity.User;
import com.in28minutes.learning.jpa.jpain10steps.service.UserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author Anja
 */
@Component
public class UserDaoServiceCommandLineRunner implements CommandLineRunner{
    
    private static final Logger log =
            LoggerFactory.getLogger(UserDaoServiceCommandLineRunner.class);
    
    @Autowired
    private UserDAOService userDAOService;
    
    
    
    @Override
    public void run(String... args) throws Exception {
        User user = new User("Jack", "Admin");
        long insert = userDAOService.insert(user);
        //New User is created: User{id=1, name=Jack, role=Admin}
        log.info("New User is created: "+user);
    }
    
}
