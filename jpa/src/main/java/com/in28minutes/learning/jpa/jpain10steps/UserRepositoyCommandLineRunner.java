/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.in28minutes.learning.jpa.jpain10steps;

import com.in28minutes.learning.jpa.jpain10steps.entity.User;
import com.in28minutes.learning.jpa.jpain10steps.service.UserDAOService;
import com.in28minutes.learning.jpa.jpain10steps.service.UserRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author Anja
 */
@Component
public class UserRepositoyCommandLineRunner implements CommandLineRunner{
    
    private static final Logger log =
            LoggerFactory.getLogger(UserRepositoyCommandLineRunner.class);
    
    @Autowired
    private UserRepository userRepository;
    
    
    
    @Override
    public void run(String... args) throws Exception {
        User user = new User("Jill", "Admin");
        userRepository.save(user);
        Optional<User> userWithIdOne = userRepository.findById(1L);
        //New User is created: User{id=1, name=Jack, role=Admin}
        log.info("User is retreived: "+userWithIdOne);
        
        List<User> users = userRepository.findAll();
        log.info("All users: "+users);
        
    }
    
}
